import React, { Fragment } from "react";
import { Query , useQuery} from "react-apollo";
import gql from "graphql-tag";
import { render } from "@testing-library/react";

const TheQuery = gql`query ($id :Int!) {
  items (ids: [$id]) {
    updates {
      body
      created_at 
      creator {
        name
      }}
    name
    id
    creator {
      name
    }
  column_values {
  id
  value
  }
  }
  }`
  function DoTheQuerry(id) {
    console.log(typeof(id.fields));
    console.log(id.fields);
    
    const { loading, error, data } = useQuery(TheQuery, {
      variables: { id: id.fields },
    });
    if (loading) return <p>Loading ...</p>;
    if (error) return <p>Error </p>;
    if (data.items[0] == null) return <p>no data</p>

    return  <Fragment>
                   
                    <div>Langer Text</div>
                    <div>{data.items[0].column_values[3].value.substring(9,data.items[0].column_values[3].value.length-42)}</div>
                    <hr/>
                    {console.log(data)
                             }
                    <div>{data.items[0].updates.map((d)=>{
                           return d.body.includes("<blockquote>") ? ( <Fragment>
                             
                             {console.log(d.body)
                             }
                             <div className="bg-light border border-secondary rounded p-3 mb-3 ml-5" >
                               <div class="small text-right mb-3 text-secondary">{d.creator.name} | {d.created_at}</div>
                               <div dangerouslySetInnerHTML={{ __html: d.body }} />
                             </div> 
                             </Fragment>   ) : null                                  
                        
                    })}</div>
                    <div>
                  <textarea className="w-100" style={{height: "100px"}}></textarea>
                  <div class="small text-right mb-3 text-secondary float-left">{data.items[0].column_values[0].value.substring(1,data.items[0].column_values[0].value.length-1)}</div>
                  <button className="btn btn-success float-right">absenden</button>
              </div>
                </Fragment>
  }
 
const Data = (id)=>(
    // id.id
    < DoTheQuerry fields={parseInt(id.id)} />

    
);

export default Data;