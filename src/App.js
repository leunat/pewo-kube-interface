import React, { Fragment } from 'react';
import logo from './logo.svg';
import apolloClient from "apollo-boost"
import {ApolloProvider} from "react-apollo"
import './App.css';
import Data from "./Data"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

const url = window.location.href.split("/")[3]


const client = new apolloClient({
  uri:'https://api.monday.com/v2/',   
  headers: {
      'Content-Type': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjM5ODA3NTE3LCJ1aWQiOjEyMTg5Mzg3LCJpYWQiOiIyMDIwLTAzLTExIDA3OjI5OjMwIFVUQyIsInBlciI6Im1lOndyaXRlIn0.xBPqUyaZqMtCAnSM24NJHF9OVKXdNFpGzbdp_8gYIiQ'
    },
    
});

function App() {
  return (
    <Router>
      <Switch>
          <Route path="/:id" >
          <div className="container">
                  <ApolloProvider client={client}>
                    <Data id={url}/>
                  </ApolloProvider>
                  
              </div>
          </Route>
          <Route path="/">
              
              <a href="/488793363"> Go to 488793363</a>
          </Route>
          
          
        </Switch>
     
              
              
                  
      
    </Router>
    
    
  );
}
function Child() {
  // We can use the `useParams` hook here to access
  // the dynamic pieces of the URL.

  
}

export default App;
